use serde::{Deserialize, Serialize, Serializer};
use std::error::Error;
use std::fmt::Display;
use std::str::FromStr;

#[derive(Deserialize, Debug, Clone, Copy)]
#[serde(try_from = "std::borrow::Cow<'_, str>")]
pub struct MacAddress {
    pub bytes: [u8; 6],
}

impl Serialize for MacAddress {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.collect_str(&self)
    }
}

impl Display for MacAddress {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{:02x}:{:02x}:{:02x}:{:02x}:{:02x}:{:02x}",
            self.bytes[0],
            self.bytes[1],
            self.bytes[2],
            self.bytes[3],
            self.bytes[4],
            self.bytes[5],
        )
    }
}

#[derive(Debug)]
pub struct MacAddressError;
impl Error for MacAddressError {}

impl std::fmt::Display for MacAddressError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "failed to parse MAC address (must be 6 hex integers, 00<=x<=FF, separated by '-' or ':')")
    }
}

impl FromStr for MacAddress {
    type Err = MacAddressError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split: Vec<&str> = s.split(|c| c == ':' || c == '-').collect();
        if split.len() != 6 {
            return Err(MacAddressError);
        }
        let mut bytes: [u8; 6] = [0; 6];
        for i in 0..6 {
            bytes[i] = match u8::from_str_radix(split[i], 16) {
                Ok(v) => v,
                Err(_) => return Err(MacAddressError),
            };
        }
        Ok(MacAddress { bytes })
    }
}

impl std::convert::TryFrom<&'_ str> for MacAddress {
    type Error = MacAddressError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        value.parse()
    }
}

impl TryFrom<std::borrow::Cow<'_, str>> for MacAddress {
    type Error = MacAddressError;
    fn try_from(value: std::borrow::Cow<'_, str>) -> Result<Self, Self::Error> {
        value.parse()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json;

    #[test]
    fn format() {
        let ma = MacAddress {
            bytes: [0xa, 0xb, 0xc, 0xd, 0xe, 0xf],
        };

        assert_eq!(format!("{}", ma), "0a:0b:0c:0d:0e:0f");
    }

    #[test]
    fn from_str() {
        let ma: MacAddress = "0A:0B:0C:0D:0E:0F".parse().unwrap();
        assert_eq!(ma.bytes, [0xa, 0xb, 0xc, 0xd, 0xe, 0xf]);

        let ma: MacAddress = "0A-0b:0C-0D:0E:0f".parse().unwrap();
        assert_eq!(ma.bytes, [0xa, 0xb, 0xc, 0xd, 0xe, 0xf]);

        "0a:0b:0c:0d:0e"
            .parse::<MacAddress>()
            .expect_err("did not raise error");
        "0A:0B:0C:0D:0E:100"
            .parse::<MacAddress>()
            .expect_err("did not raise error");
        "0A:0B:0C:0D:xy"
            .parse::<MacAddress>()
            .expect_err("did not raise error");
    }

    #[test]
    fn serialize() {
        let ma = MacAddress {
            bytes: [0xa, 0xb, 0xc, 0xd, 0xe, 0xf],
        };

        assert_eq!(
            serde_json::to_string(&ma).unwrap(),
            r#""0a:0b:0c:0d:0e:0f""#
        );
    }

    #[test]
    fn deserialize() {
        let ma: MacAddress = serde_json::from_str(r#""0A:0B-0C:0D:0E:0F""#).unwrap();
        assert_eq!(ma.bytes, [0xa, 0xb, 0xc, 0xd, 0xe, 0xf]);
    }
}
