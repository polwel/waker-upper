use actix_web::{middleware::Logger, web, App, HttpServer};
use log::{error, info};

use std::process;
use std::sync::Arc;

mod api;
mod config;
mod index;
mod mac;
mod wol;

use api::{list, wake};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "info");
    //std::env::set_var("RUST_LOG", "actix_web=info");
    //std::env::set_var("RUST_BACKTRACE", "1");
    env_logger::init();

    let cfg = Arc::new(match config::read_config() {
        Err(config::ConfigError::File(e)) => {
            error!("Failed to open config.yaml: {e}");
            process::exit(1);
        }
        Err(config::ConfigError::Read(e)) => {
            error!("Failed to parse config.yaml: {e}");
            process::exit(1);
        }
        Ok(c) => c,
    });

    let http_port = cfg.http_port;
    let http_host = cfg.http_host;

    let server = HttpServer::new(move || {
        let api = web::scope("/api").service(list).service(wake);
        App::new()
            .app_data(web::Data::new(cfg.clone()))
            .wrap(Logger::default())
            .service(index::index)
            .service(api)
    })
    .bind((http_host, http_port))
    .unwrap_or_else(|e| {
        println!("Failed to bind socket: {e}");
        process::exit(1);
    })
    .run();
    info!("Serving webinterface at http://{http_host}:{http_port}");
    server.await
}
