use actix_web::{get, post, web, HttpResponse, Responder};
use serde::Serialize;
use std::sync::Arc;

use crate::{config, mac::MacAddress, wol::send_wol};
use log::{info, warn};

#[get("/list")]
pub async fn list(data: web::Data<Arc<config::Config>>) -> impl Responder {
    #[derive(Serialize)]
    struct Device {
        index: usize,
        host_name: String,
        mac_address: MacAddress,
    }

    #[derive(Serialize)]
    struct DeviceList {
        devices: Vec<Device>,
    }

    let mut device_list = Vec::new();
    for (i, dev) in data.devices.iter().enumerate() {
        device_list.push(Device {
            index: i,
            host_name: dev.name.clone(),
            mac_address: dev.mac_address,
        })
    }
    let devices = DeviceList {
        devices: device_list,
    };
    HttpResponse::Ok()
        .content_type("application/json")
        .body(serde_json::to_string(&devices).unwrap())
}

#[post("/wake/{device_index}")]
pub async fn wake(data: web::Data<Arc<config::Config>>, path: web::Path<u32>) -> impl Responder {
    let device_index = path.into_inner();
    let device = match data.devices.get(device_index as usize) {
        Some(d) => d,
        None => {
            return HttpResponse::NotFound()
                .content_type("text/plain")
                .body("Not Found")
        }
    };
    info!(
        "Sending WOL packet to {} ({})...",
        device.name, device.mac_address
    );
    match send_wol(
        device.mac_address,
        data.broadcast_ip,
        Some(data.broadcast_port),
    ) {
        Ok(_) => {
            return HttpResponse::Ok()
                .content_type("application/json")
                .body(format!(
                    "
            {{
                \"success\": true,
                \"message\": \"Sent WOL packet to {} ({})\"
            }}
            ",
                    device.name, device.mac_address
                ))
        }
        Err(e) => {
            warn!("{e}");
            return HttpResponse::Ok()
                .content_type("application/json")
                .body(format!(
                    "
            {{
                \"success\": false,
                \"message\": \"Failed to send WOL packet to {} ({}). Check configuration.\"
            }}
            ",
                    device.name, device.mac_address
                ));
        }
    }
}
