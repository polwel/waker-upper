use actix_web::HttpResponse;
use actix_web::{get, web, Responder};
use serde::Serialize;
use std::sync::Arc;
use std::vec::Vec;
use tinytemplate::TinyTemplate;

use crate::config;

#[get("/")]
async fn index(data: web::Data<Arc<config::Config>>) -> impl Responder {
    let template = include_str!("index.html");

    let mut tt = TinyTemplate::new();
    match tt.add_template("index", template) {
        Ok(_) => (),
        Err(e) => {
            println!("{e}");
            return HttpResponse::InternalServerError()
                .content_type("text/html")
                .body("Internal server error");
        }
    };
    #[derive(Serialize)]
    struct Context<'a> {
        devices: &'a Vec<config::Device>,
    }

    let context = Context {
        devices: &data.devices,
    };

    return match tt.render("index", &context) {
        Ok(rendered) => HttpResponse::Ok().content_type("text/html").body(rendered),
        Err(e) => {
            println!("Error rendering template: {e}");
            HttpResponse::InternalServerError()
                .content_type("text/html")
                .body("Internal server error")
        }
    };
}
