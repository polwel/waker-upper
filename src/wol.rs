use crate::mac::MacAddress;
use std::{
    io::Error,
    net::{IpAddr, SocketAddr, UdpSocket},
};

pub fn send_wol(
    target: MacAddress,
    broadcast_ip: IpAddr,
    broadcast_port: Option<u16>,
) -> Result<(), Error> {
    let bind_address = "0.0.0.0:0";
    let broadcast_address = SocketAddr::new(broadcast_ip, broadcast_port.unwrap_or(9));
    let socket: UdpSocket = UdpSocket::bind(bind_address)?;
    socket.set_broadcast(true)?;
    let mut payload: Vec<u8> = vec![0xff; 6];

    for _ in 0..16 {
        payload.extend_from_slice(&target.bytes);
    }

    socket.send_to(&payload, broadcast_address)?;

    Ok(())
}
