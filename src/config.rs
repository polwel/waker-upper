use crate::mac::MacAddress;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io;
use std::net::IpAddr;
use std::result::Result;
use std::string::String;
use std::vec::Vec;

#[derive(Debug)]
pub enum ConfigError {
    File(io::Error),
    Read(serde_yaml::Error),
}

#[derive(Serialize, Deserialize)]
pub struct Device {
    pub name: String,
    pub mac_address: MacAddress,
}

#[derive(Serialize, Deserialize)]
pub struct Config {
    pub devices: Vec<Device>,
    pub http_port: u16,
    pub http_host: IpAddr,
    pub broadcast_ip: IpAddr,
    pub broadcast_port: u16,
}

pub fn read_config() -> Result<Config, ConfigError> {
    let f = match File::open("config.yaml") {
        io::Result::Ok(f) => Ok(f),
        io::Result::Err(e) => Err(ConfigError::File(e)),
    }?;
    let reader = io::BufReader::new(f);

    match serde_yaml::from_reader(reader) {
        serde_yaml::Result::Ok(config) => Ok(config),
        serde_yaml::Result::Err(e) => Err(ConfigError::Read(e)),
    }
}
