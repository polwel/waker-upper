# Waker-Upper

Waker-Upper is a wake-on-LAN (WOL) server. It provides a HTTP interface (HTML & REST) for transmitting WOL packets to other devices reachable by the server.

Why would you want to do this? Perhaps you are separated by a firewall from the device you are trying to reach, and can only talk to it via an IP address. Devices that sleep do not have an IP.

## Usage

Waker-Upper looks for a `config.yaml` in the current directory that lists the devices it can wake. The format should be quite self-explanatory. You can find an example below.

Waker-Upper emits the WOL signal as a UDP broadcast packet. The destination IP and port can be set in the config file (`broadcast_ip` and `broadcast_port`). This in principle allows routing the WOL signal, if you wanted to do that for whatever reason.

Then, simply run Waker-Upper. On Linux:
```
# ./wakerupper
```

Or Windows:
```
$ wakerupper.exe
```

It will parse the config file, and boot the webserver.

```
$ wakerupper.exe
[...]
[...] Serving webinterface at http://127.0.0.1:8080
```

Open a browser and go have a look!

## Example config file

```yaml
# config.yaml
devices:
- name: Device1
  mac_address: ca:fe:ba:fe:00:01
- name: Some other device
  mac_address: de:ad:be:ef:ca:fe
- name: Host 3
  mac_address: de:ad:be:ef:ca:fe
broadcast_ip: 10.1.255.255
broadcast_port: 9
http_host: 127.0.0.1
http_port: 8080
```

## Screenshot
![screenshot](screenshot.png)