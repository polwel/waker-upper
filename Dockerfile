FROM rust:latest


RUN apt-get update && apt-get install -y gcc-mingw-w64-x86-64 curl && rm -rf /var/lib/apt/lists/*
RUN cargo install cargo-binstall
RUN cargo binstall --no-confirm cargo-tarpaulin cargo-nextest cargo-audit && rm -rf $/.cargo/registry
RUN rustup component add rustfmt
RUN rustup component add clippy
RUN rustup target add x86_64-pc-windows-gnu
# RUN rustup target add x86_64-apple-darwin